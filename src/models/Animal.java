package models;

public class Animal {

    private int animalId;
    private String name;
    private AnimalType animalType;
    private String disease;
    private int triage;

    public Animal(int animalId, String name, AnimalType animalType, String disease, int triage) {
        this.animalId = animalId;
        this.name = name;
        this.animalType = animalType;
        this.disease = disease;
        this.triage = triage;
    }

    public Animal() {
    }

    public int getAnimalId() {
        return animalId;
    }

    public void setAnimalId(int animalId) {
        this.animalId = animalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }

    public void setAnimalType(AnimalType animalType) {
        this.animalType = animalType;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public int getTriage() {
        return triage;
    }

    public void setTriage(int triage) {
        this.triage = triage;
    }

    @Override
    public String toString() {
        return  "\nAnimal [" + animalId + "]{" +
                "\n Nombre: " + name +
                "\n Tipo: " + animalType +
                "\n Enfermedad: " + disease +
                "\n Triage: " + triage +
                '}';
    }
}
