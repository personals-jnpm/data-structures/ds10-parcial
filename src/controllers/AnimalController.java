package controllers;

import models.Animal;
import models.AnimalType;

public class AnimalController {

    private final Animal[] animals;
    private int positionAnimal;

    public AnimalController() {
        positionAnimal = 6;
        animals = new Animal[10];
        initAnimals();
    }

    public void addAnimal(Animal animal){
        animal.setAnimalId(positionAnimal);
        animals[positionAnimal] = animal;
        positionAnimal++;
    }

    public void sortInsertionMethodAnimals() {
        try {
            for (int i = 1; i < positionAnimal; i++) {
                Animal aux = animals[i];
                for (int j = i - 1; j >= 0; j--) {
                    assert animals[j] != null;
                    if (aux.getTriage() < animals[j].getTriage()) {
                        animals[j + 1] = animals[j];
                        animals[j] = aux;
                    }
                }
            }
            printAnimalsNames();
        } catch (NullPointerException ignored) {
        }
    }

    public void printAnimalsNames(){
        try {
            System.out.print("\n");
            for (int i = 0; i < positionAnimal; i++) {
                System.out.print("Animal["+animals[i].getAnimalId()+"] "+animals[i].getName() +
                        ", Triage: "+ animals[i].getTriage()+" \n");
            }
        } catch (NullPointerException e) {
            System.out.println("  ¡No hay animales en la lista!");
        }
    }

    public Animal getAnimalByName(String name) throws NullPointerException{
            for (Animal animal : animals) {
                if (animal.getName().equals(name)) {
                    return animal;
                }
            }
        return null;
    }


    public void printAnimals() {
        try {
            System.out.print(" ");
            for (int i = 0; i < positionAnimal; i++) {
                System.out.print(animals[i] + " \n");
            }
        } catch (NullPointerException e) {
            System.out.println("  ¡No hay animales en la lista!");
        }
    }

    public void initAnimals(){
        animals[0] = new Animal(0, "Tom", AnimalType.Gato, "Moquillo", 5);
        animals[1] = new Animal(1, "Guardián", AnimalType.Perro, "Rabia", 1);
        animals[2] = new Animal(2, "Plumón", AnimalType.Ave, "Psitacosis", 2);
        animals[3] = new Animal(3, "Toby", AnimalType.Perro, "Fiebre", 3);
        animals[4] = new Animal(4, "Mustafá", AnimalType.Gato, "Brucelosis", 4);
        animals[5] = new Animal(5, "Colorín", AnimalType.Ave, "Criptococosis", 2);
    }

}
