import controllers.AnimalController;
import models.Animal;
import models.AnimalType;
import utiliy.Keyboard;

public class Main {

    public static Keyboard keyboard = new Keyboard();
    public static AnimalController animalController = new AnimalController();

    public static void main(String[] args) {
        Main main = new Main();

        main.menu();
        int option;
        do {
            System.out.print("\nSeleccione una opción en el menú principal: ");
            option = keyboard.readIntegerDefault(-1);
            switch (option) {
                case 0 -> System.out.println(" El programa ha finalizado");
                case 1 -> main.addAnimal();
                case 2 -> main.sortAnimalByTriage();
                case 3 -> main.searchAnimalByName();
                case 4 -> main.printAnimals();
                default -> System.out.println(" ¡Opción no disponible en el menú principal!");
            }
        } while (option != 0 );
    }

    public void menu() {
        System.out.println("╔═════════════════════════════════════════════════════╗");
        System.out.println("╠---------------------Veterinario---------------------╣");
        System.out.println("║═════════════════════════════════════════════════════║");
        System.out.println("║   1. Agregar animal                                 ║");
        System.out.println("║   2. Ordenar animales por triage                    ║");
        System.out.println("║   3. Buscar animal por nombre                       ║");
        System.out.println("║   4. Imprimir animales                              ║");
        System.out.println("║   0. Salir                                          ║");
        System.out.println("╚═════════════════════════════════════════════════════╝");
    }

    public void addAnimal(){
        Animal animal = new Animal();
        System.out.print("  Ingrese el nombre de la mascota: ");
        animal.setName(keyboard.readLine());
        animal.setAnimalType(getAnimalType(
                keyboard.readValidPositiveInteger("  Ingrese el tipo de animal(0: Gato, 1: Perro, 2: Ave): ")));
        System.out.print("  Ingrese la enfermedad del animal: ");
        animal.setDisease(keyboard.readLine());
        animal.setTriage(keyboard.readValidTriage());

        animalController.addAnimal(animal);
    }

    private AnimalType getAnimalType(int option) {
        switch (option) {
            case 0 -> {
                return AnimalType.Gato;
            }
            case 1 -> {
                return AnimalType.Perro;
            }
            case 3 ->{
                return AnimalType.Ave;
            }
            default -> {
                System.out.println(" ¡Opción no disponible!");
                return AnimalType.SinDefinir;
            }
        }
    }

    public void sortAnimalByTriage(){
        animalController.sortInsertionMethodAnimals();
    }

    public void searchAnimalByName(){
        try{
            System.out.print("  Ingrese el nombre del animal: ");
            Animal animal = animalController.getAnimalByName(keyboard.readLine());
            System.out.println(animal.toString());
        }catch (NullPointerException e){
            System.out.println("  ¡No se encontró un animal con ese nombre!");
        }
    }

    public void printAnimals(){
        animalController.printAnimals();
    }

}
